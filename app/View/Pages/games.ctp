<div class="container">
    <?php

    function makeGame($o, $name, $title)
    {
        echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">';
        echo '<div class="thumbnail" style="position: relative; width: 200px; height: ';
        if (AuthComponent::user('id') != 0) {
            echo '300px">';
        } else {
            echo '260px">';
        }
        echo $o->Html->link($title, '#',
            array('class' => 'btn btn-warning btn-block', 'escape' => false,
                'style' => 'left:0'));
        echo '<br>';
        echo $o->Html->image($name . '.jpg', array('max-width' => '100%'));
        if (AuthComponent::user('id') != 0) {
            echo '<br>';
            echo $o->Html->link('play',
                array('controller' => 'games', 'action' => 'index', 'game' => $title),
                array('class' => 'btn btn-primary btn-block active', 'escape' => false,
                    'style' => 'position: absolute; left:0; bottom: 0'));
        }
        echo '</div>';
        echo '</div>';
    }

    echo '<div class="row" style="padding: 10px">';
    makeGame($this, 'dvonn', 'Dvonn');
    makeGame($this, 'invers', 'Invers');
    makeGame($this, 'gipf', 'Gipf');
    makeGame($this, 'kamisado', 'Kamisado');
    echo '</div>';
    echo '<div class="row" style="padding: 10px">';
    makeGame($this, 'tzaar', 'Tzaar');
    makeGame($this, 'yinsh', 'Yinsh');
    makeGame($this, 'zertz', 'Zertz');
    echo '</div>';
    ?>

</div>