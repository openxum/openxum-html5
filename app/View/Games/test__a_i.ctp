<div class="container">

    <?php
    echo $this->Html->script('utils/utils');
    echo $this->Html->script('MCTS/MCTS');
    echo $this->Html->script('MCTS/Node');
    echo $this->Html->script('MCTS/Player');
    echo $this->Html->script('kamisado/Kamisado');
    echo $this->Html->script('kamisado/Constants');
    echo $this->Html->script('kamisado/Engine');
    echo $this->Html->script('kamisado/AIManager');
    echo $this->Html->script('kamisado/RandomPlayer');
    echo $this->Html->script('kamisado/FlatMCTSPlayer');
    echo $this->Html->script('kamisado/MCTSPlayer');
    ?>

    <script language="javascript">
        var N = 100;
        var i;
        var black_victories;
        var white_victories;

        function start() {
            i = 1;
            black_victories = 0;
            white_victories = 0;
            $('.progress-bar').width(i + '%');
            $('.progress-bar').text(i + '%');
            $('.progress').addClass('active');
            setTimeout(run, 1000);
        }

        function run() {
            var engine = new Kamisado.Engine(0, 0);
            var black = new Kamisado.FlatMCTSPlayer(Kamisado.Color.BLACK, engine);
            var white = new Kamisado.MCTSPlayer(Kamisado.Color.WHITE, engine, 500);
            var manager = new Kamisado.AIManager(engine, black, white);

//            console.log("START");

            manager.eval();
            if (engine.winner_is() == Kamisado.Color.BLACK) {
                ++black_victories;
            } else {
                ++white_victories;
            }
            $('.progress-bar').width(i + '%');
            $('.progress-bar').text(i + '%');
            document.getElementById("#results").innerHTML = 'Results: ' + black_victories + ' black victories and ' +
                white_victories + ' white victories <br>';
            ++i;
            if (i <= N) {
                setTimeout(run, 15);
                $('.progress').removeClass('active');
            } else {
                clearInterval(run);
            }
        };
    </script>

    <?php
    echo $this->Html->link('start', 'javascript:start();',
        array('class' => 'btn btn-warning btn-block', 'escape' => false));
    echo '<br>';
    ?>

    <div class="progress progress-striped">
        <div class="progress-bar" style="width: 0%;"></div>
    </div>

    <div id="#results"></div>

</div>